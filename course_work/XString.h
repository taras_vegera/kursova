#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
#include <iomanip>
#include <string>
using namespace std;




class XString{
public:
	XString();	// default constructor
	XString(const char * str);	// cosntructor
	XString(const XString & arg);	// copying constructor
	void SetStr(char const * str);	// SetStr
	void SetStr(string str);
	void Show();	// show str
	~XString();	// destructor
	char * getstr();
	XString operator * (XString & arg);	// the same letters in words
	XString operator / (XString & arg);	// letters  that exist in left argument and not exist in right
	XString operator + (XString & arg);	// concat strings
	friend XString operator + (const char *str,XString & arg); // concat (const char * + XString & arg)
	friend XString operator + (XString & arg,const char *str); // concat (XString & arg + const char *)
	XString operator = (XString & arg);	// =
	XString operator = (const char * arg);	// =
	XString operator *= (XString & arg); // *=
	XString operator /= (XString & arg); // /=
	XString operator += (XString & arg);

	
	
	//logical operators
	bool operator < (XString & arg);	//return true if (this < arg)
	bool operator > (XString & arg);	//return true if (this > arg)
	bool operator == (XString & arg);	//return true if (this == arg)
	bool operator != (XString & arg);	//return true if (this != arg)
	bool operator <= (XString & arg);	//return true if (this <= arg)
	bool operator >= (XString & arg);	//return true if (this >= arg)

	operator char * ();					//XString to char*
	XString operator ! ();				//recerse

	friend ostream & operator << ( ostream & outStream , const XString & obj );
	friend istream & operator >> ( istream & iStream ,  XString & obj ); 
	char & operator [] (const int n);
	XString operator () (const int idx, const int amount);


private:
	char *str;
};

