#include "XString.h"

XString :: XString()
{
	str=new char (0);
} // default constructor

XString :: XString(char const * str) : str(0){
SetStr(str);
} // cosntructor

XString :: XString(const XString & arg): str(0){
SetStr(arg.str);
} // copying constructor

void XString :: SetStr(char const * str){
	delete [] this->str;
	this->str= new char [strlen(str)+1];
	strcpy(this->str,str);
} // SetStr

void XString :: SetStr(string str)
{
	delete [] this->str;
	this->str = new char [str.size()+1];
	for (int i = 0; i < str.size(); i++ )
		this->str[i]=str[i];
	this->str[str.size()] = 0;
}

void XString :: Show(){
puts(this->str);
} // show str

XString :: ~XString(){
delete [] this->str;
} // destructor
	

XString XString :: operator * (XString & arg){
int length=strlen(this->str);
char * temp;
temp = new char [length+1];
int index=0;
	for (int i=0;i<length;i++){

		bool flag=true;							//checkup the same letters in temp
		for (int j=0;j<index;j++)
			if (temp[j]==this->str[i])
				flag=false;



		if(strchr(arg.str,this->str[i]) && flag) {
		temp[index]=this->str[i];
		index++;
		}
	}
temp[index]=0;
	return XString (temp);
} // (operator *) the same letters in words


XString XString :: operator / (XString & arg){
int length=strlen(this->str);
char * temp;
temp = new char [length+1];
int index=0;
	for (int i=0;i<length;i++){
		
		bool flag=true;							//checkup the same letters in temp
		for (int j=0;j<index;j++)
			if (temp[j]==this->str[i])
				flag=false;
		
				
		if(!strchr(arg.str,this->str[i]) && flag) {
		temp[index]=this->str[i];
		index++;
		}
	}
temp[index]=0;
	return XString (temp);
} // (operator /) letters  that exist in left argument and not exist in right


XString XString :: operator + (XString & arg){
	char *temp;
	temp=new char [strlen(this->str)+strlen(arg.str)+1];
	strcpy(temp,this->str);
	strcat(temp,arg.str);
	return XString(temp);
} //(operator +) concat strings




XString XString :: operator *= (XString & arg){

XString  t(*(this)*(arg));
strcpy(this->str,t.str);
return *this;
}

XString XString :: operator /= (XString & arg){
XString  t(*(this)/(arg));
strcpy(this->str,t.str);
return *this;
}



bool XString :: operator < (XString & arg){
	if(strcmp(this->str,arg.str)<0) 
		return true;
	else if(strcmp(this->str,arg.str)>0)
		return false;
}

bool XString :: operator > (XString & arg){
	if(strcmp(this->str,arg.str)<0) 
		return false;
	else if(strcmp(this->str,arg.str)>0) 
		return true;
}


bool XString :: operator == (XString & arg){
	return(!strcmp(this->str,arg.str));
}


bool XString :: operator != (XString & arg){
	return(!(*this==arg));
}

bool XString :: operator <= (XString & arg){
	return(*this<arg || !strcmp(this->str,arg.str));

}

bool XString :: operator >= (XString & arg){
	return(*this>arg || !strcmp(this->str,arg.str));
}



XString :: operator char* (){
	return (this->str);
}


XString XString :: operator ! (){
	char t;
	for (int i=0;i<strlen(this->str)/2;i++){
	t=this->str[i];
	this->str[i]=this->str[strlen(this->str)-i-1];
	this->str[strlen(this->str)-i-1]=t;
	}
	return *this;

}





XString XString :: operator = (XString & arg){
	delete [] this->str;
	this->str= new char [strlen(arg.str)+1];
strcpy(this->str,arg.str);

return *this;
}

XString XString :: operator = (const char * arg){
	delete [] this->str;
	this->str= new char [strlen(arg)+1];
strcpy(this->str,arg);
return *this;
}

XString  operator + (const char *str,XString & arg){
XString temp(str);
return (temp+arg);
}

XString  operator + (XString & arg,const char *str){
XString temp(str);
return (arg+temp);
}


ostream & operator<< ( ostream & outStream , const XString & obj )
{
	outStream << obj.str;
	return outStream;
}

istream & operator >> (istream & iStream ,  XString & obj ){
	char * temp= new char [50];
	iStream >> temp;
	obj.str=temp;
	return iStream;
}

XString XString :: operator += (XString & arg){
return (*this=*this+arg);
}


char & XString :: operator [] (const int n){
	if (n<0 || n>=strlen(this->str)){
	cout<<"Error\n";
	exit(1);
	}
	else return this->str[n];
}




XString XString :: operator () (const int idx, const int amount){
	if (idx<0 || idx+amount>=strlen(this->str)){
	cout<<"Error\n";
	exit(1);
	}
	char * temp;
	temp= new char [idx+amount+1];
	for (int i=idx,j=0;i<idx+amount;i++,j++){
	temp[j]=this->str[i];
	}
	temp[amount]=0;
	return XString(temp);

}
char * XString :: getstr(){
	return this->str;
}