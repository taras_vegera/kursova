#pragma once
#include "XString.h"
#include "windows.h"

enum Color
{
	Black = 0,
	Grey = FOREGROUND_INTENSITY,
	LightGrey = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
	White = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	Blue = FOREGROUND_BLUE,
	Green = FOREGROUND_GREEN,
	Cyan = FOREGROUND_GREEN | FOREGROUND_BLUE,
	Red = FOREGROUND_RED,
	Purple = FOREGROUND_RED | FOREGROUND_BLUE,
	LightBlue = FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	LightGreen = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
	LightCyan = FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	LightRed = FOREGROUND_RED | FOREGROUND_INTENSITY,
	LightPurple = FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
	Orange = FOREGROUND_RED | FOREGROUND_GREEN,
	Yellow = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
};
enum Background
{
	BLACK = 0,
	GREY = BACKGROUND_INTENSITY,
	LIGHTGREY = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE,
	WHITE = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY,
	BLUE = BACKGROUND_BLUE,
	GREEN = BACKGROUND_GREEN,
	CYAN = BACKGROUND_GREEN | BACKGROUND_BLUE,
	RED = BACKGROUND_RED,
	PURPLE = BACKGROUND_RED | BACKGROUND_BLUE,
	LIGHTBLUE = BACKGROUND_BLUE | BACKGROUND_INTENSITY,
	LIGHTGREEN = BACKGROUND_GREEN | BACKGROUND_INTENSITY,
	LIGHTCYAN = BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY,
	LIGHTRED = BACKGROUND_RED | BACKGROUND_INTENSITY,
	LIGHTPURPLE = BACKGROUND_RED | BACKGROUND_BLUE | BACKGROUND_INTENSITY,
	ORANGE = BACKGROUND_RED | BACKGROUND_GREEN,
	YELLOW = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_INTENSITY,
};
class Informer : public XString{
public : 

	Informer(const char * newInf);
	Informer(const char * newInf,unsigned x,unsigned y,const char symbol,const unsigned Nmax_wight);
	Informer(const char * newInf,unsigned x,unsigned y,const char symbol,const unsigned Nmax_wight,Color c,Background b);


	void SetPosition(unsigned x,unsigned y);																		//set current cursor position
	void SetMainPosition(unsigned x,unsigned y);																	//set main position
	void print();																								//print text with border
	void PrintWithoutBorder();																				//print text without border
	void SetBackground(Background Nbackground);																	//set BackGround color (input data (enum BackGround))
	void SetBackground(char * str);																				//set backgound color (input data (const char *))
	void SetColor(Color Ncolor);																				//set font color (input data (enum Color))
	void SetColor(char * str);																					//set font color (input data (const char *))
	void update();																								//update data
	size_t getX();																								//return X coord of main position
	size_t getY();																								//return Y coord of main position
	Color CheckColor (char * inString);																		//transfer color (const char * to enum Color)
	Background CheckBackground(char * inString);																//transfer backGround (const char * to enum backGround)
	void AddChar(char symbol);																			// set border symbol
	Informer operator = (Informer & arg);
	Informer operator = (const char * line);
	Informer operator = (char symbol);
	Informer operator += (Informer & arg);
	Informer operator += (const char * line);
	Informer operator += (const char symbol);

private:
	COORD Cpos;
	COORD MAIN_pos;
	unsigned max_wight;
	char symbol;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	HANDLE hCon;
	WORD font_color;
	WORD background;
};

