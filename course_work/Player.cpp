#include "Player.h"



Player :: Player(): i_score(0), Player_name("-"), score("0")
{
	this->Player_name.AddChar(char(177));
	this->score.AddChar(char(177));
	this->Player_name.SetColor(Black);
	this->Player_name.SetBackground(GREY);
	this->score.SetColor(Black);
	this->score.SetBackground(GREY);
	this->password = "";
}
Player::Player(const char * pl_name, const size_t scr, const char * plr_password) : Player_name(pl_name, 61, 10, (char)177, 11), score("0", 70, 15, (char)177, 11)
{
	this->i_score=0;
	this->Player_name.SetBackground(GREY);
	this->score.SetBackground(GREY);
	this->SetScore(scr);
	this->password = plr_password;
	this->CanContinue = false;
}																						//default constructor

void Player :: SetScore(const size_t Score)
{	
	this->i_score+=Score;
	char buffer[100];
	this->score.SetStr(_itoa(this->i_score,buffer,10));
}																						// set Player score


void Player :: PrintName()
{
	this->Player_name.print();
}

void Player :: PrintScore()
{
	this->score.print();
}

void Player :: SetNamePos(size_t x, size_t y)
{
	this->Player_name.SetMainPosition(x,y);
}

void Player :: SetScorePos(size_t x, size_t y)
{
	this->score.SetMainPosition(x,y);
}



ofstream & operator << (ofstream & out, Player & obj )
{
	size_t strLen = strlen((const char * )(obj.Player_name.getstr())) + 1;									// save the lenght of str
	
	out.write( (const char * ) &strLen, sizeof( strLen )  );				//write into file the size of str

	out.write( obj.Player_name.getstr(), strLen );										//write into file the str

	out.write( (const char * ) &obj.i_score, sizeof( obj.i_score) );			//write into file the price

	out.write((const char *)&obj.CanContinue, sizeof(obj.CanContinue));			//write into file the price
	
	size_t strPass = strlen((const char *)(obj.password.getstr())) + 1;
	
	out.write((const char *)&strPass, sizeof(strPass));				//write into file the size of str
	
	out.write(obj.password.getstr(), strPass);										//write into file the str

	return out;
}

void Player ::  SetName(const char * name)
{
	this->Player_name=name;
}
void Player::SetPassword(const char * password)
{
	this->password = password;
}

ifstream & operator >> (ifstream & in, Player & obj )
{

	size_t nameSize;

	in.read( (char*) &nameSize, sizeof(nameSize) );

	char * temp = new char [nameSize];

	in.read( temp, nameSize );

	obj.SetName(temp);

	delete [] temp;

	in.read( (char*) &obj.i_score, sizeof(obj.i_score) );
	in.read((char*)&obj.CanContinue, sizeof(obj.CanContinue));
	obj.FromIntToInf();
	
	size_t pSize;
	
	in.read((char*)&pSize, sizeof(pSize));
	
	char * tempPass = new char[pSize];
	
	in.read(tempPass, pSize);

	obj.SetPassword(tempPass);

	return in;

}	

void Player :: FromIntToInf(){
	char buffer[100];
	this->score.SetStr(_itoa(this->i_score,buffer,10));
}
void Player :: SetContinue( bool flag)
{
	this->CanContinue = flag;
}


void Player :: PrintPlayer(size_t x,size_t y)
{
	this->Player_name.SetMainPosition(x,y);
	this->score.SetMainPosition(x+18,y);
	this->Player_name.print();
	this->score.print();
}
size_t Player :: getX()
{
	return this->Player_name.getX();
}

char * Player :: getName()
{
	return this->Player_name.getstr();
}

char * Player::GetPassword()
{
	return this->password.getstr();
}

size_t Player :: getY()
{
	return this->Player_name.getY();
}

void Player :: SetColor(Color color)
{
	this->Player_name.SetColor(color);
	this->score.SetColor(color);
}
bool Player::GetContinue()
{
	return this->CanContinue;
}

Player Player :: operator = (Player & arg)
{
	this->SetName(arg.Player_name);
	this->i_score=arg.i_score;
	this->FromIntToInf();
	return *this;
}


size_t Player :: GetiScore()
{
	return this->i_score;
}