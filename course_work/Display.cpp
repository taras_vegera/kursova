#include "Display.h"
#include "Tube.h"
#include <conio.h>



Display :: Display(): 
screen(0),
	field(0),
	game_name(0),
	game_over(1),
	dPlayer_ptr(0)
{}// default constructor


Display :: Display(const size_t screen_height, 
	const size_t screen_width,
	const size_t field_height, 
	const size_t field_width, 
	char * Scolor,
	char * SBGcolor,
	char * Fcolor,
	char * FBGcolor
	): 
screen(" ",0,0,(char)177,screen_width),
	field(" ",0,0,(char)177,field_width),
		game_name("           Flappy Bird           ",61,3,(char)177,11),
		game_over(1),
		dPlayer_ptr(0)

{
	char * tempScreen = new char [screen_height*screen_width+1];
	char * tempField = new char [field_height*field_width+1];

	for (int i=0; i<screen_height*screen_width; i++)
		tempScreen[i]=' ';
	tempScreen[screen_height*screen_width]=0;


	for (int i=0; i<field_height*field_width; i++)
		tempField[i]=' ';
	tempField[field_height*field_width]=0;

	this->screen.SetColor(Scolor);
	this->screen.SetBackground(SBGcolor);

	this->field.SetColor(Fcolor);
	this->field.SetBackground(FBGcolor);


	this->screen.SetMainPosition(1,1);
	this->field.SetMainPosition(4,3);

	this->screen.SetStr(tempScreen);
	this->field.SetStr(tempField);






	delete []tempField;
	delete []tempScreen;


}
void Display :: SetDPlayerPtr(Player * ptr)
{
	this->dPlayer_ptr=ptr;
}

void Display :: print()
{
	
	this->screen.SetColor(Black);
	this->screen.print();
	this->field.SetColor(Blue);
	this->field.print();
	this->game_name.SetBackground(LIGHTBLUE);
	this->game_name.print();
	Bird Bird("+","Green","LIGHTCYAN",1,1);
	Bird=1;
	Bird.SetColor(White);
	Bird.SetBackground(ORANGE);
	this->dPlayer_ptr->SetNamePos(61,10);
	this->dPlayer_ptr->SetScorePos(70,15);
	this->dPlayer_ptr->PrintName();
	Informer score("SCORE:",61,15,char(177),7);
	score.SetBackground(GREY);
	score.print();
	this->dPlayer_ptr->PrintScore();
	Bird.SetMainPosition(15,10);
	Bird.PrintWithoutBorder();
	Tube * Tubes = new Tube [5];															//array of 5 Tubes
	for(int i=0; i < 5; i++){
		Tubes[i].SetBGTube(GREEN);
		Tubes[i].SetBirdPtr(&Bird);
		Tubes[i].SetPlayerPtr(this->dPlayer_ptr);
	}
	int interval=50;
	int counter=4;
	bool indicator_array[5];
	bool isC[5];
	for (int i = 0; i < 5 ; i++ ){
		indicator_array[i]=false;
		isC[i]=true;
	}

	int i=_getch();

	int last = 0;
	do {

		
		if (_kbhit())
			i=_getch();

		if (i==' ')
		{
			Bird.move("Up",2,1);
		}
		else
		{
		Bird.move("Down",1,1);
		}
		if (i==' ')
			i=1;

		if (!(interval%10) && interval!=0 )
		{
			
		Tubes[counter].SetTubePosition(50,3);
		Tubes[counter].PrintTube();
		indicator_array[counter]=true;
		counter--;
		}
		if (interval)
		interval--;
		if (indicator_array[0] && indicator_array[1] && indicator_array[2] && indicator_array[3] && indicator_array[4]){
		for (int i = 0 ; i<5 ; i++)
			isC[i]=Tubes[i].move("Left",1,1);
		}
		else{
		if (indicator_array[0])		isC[0]=Tubes[0].move("Left",1,1);
		if (indicator_array[1])		isC[1]=Tubes[1].move("Left",1,1);
		if (indicator_array[2])		isC[2]=Tubes[2].move("Left",1,1);
		if (indicator_array[3])		isC[3]=Tubes[3].move("Left",1,1);
		if (indicator_array[4])		isC[4]=Tubes[4].move("Left",1,1);
		}

		if (i == 27)
		{
			this->dPlayer_ptr->SetContinue(true);
			last = 1;
		}
	Sleep(300);
	}while(i!=27 && isC[0] && isC[1] && isC[2] && isC[3] && isC[4]);
	if (!last)
		this->dPlayer_ptr->SetContinue(false);
	Informer final_score("                                                                                                                                                                                                        ",25,5,char(177),28);
	final_score.SetBackground(GREY);
	this->dPlayer_ptr->SetNamePos(29,8);
	this->dPlayer_ptr->SetScorePos(47,8);
	final_score.print();
	this->dPlayer_ptr->PrintName();
	this->dPlayer_ptr->PrintScore();
	Sleep(1000);
}







