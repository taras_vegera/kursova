#pragma once 
#include "Informer.h"
#include "Player.h"
#include "Display.h"
#include <vector>
#include <io.h>
#include <conio.h>




class Menu 
{
public :
	Menu ();
	void print_Menu();
	void StartNewGame();
	Player * GetPlayer();
	void ContinueAndHigthScores();
private : 
Informer header;
Informer button_new_game;
Informer button_continue_game;
Informer button_exit;
Player * mPlayer_ptr;
};