#include "Tube.h"



Tube :: Tube(const char * newInf) : Bird(newInf , "White" , "WHITE",1,1) ,
	header_Tube (" ","White","WHITE",1,1),
	Bird_ptr(0),
	Player_ptr(0),
	coin(" ","YELLOW","YELLOW",1,1)
{
	this->Widht=1;
	this->Heigth=1;
}

Tube :: Tube (const char symbol , const size_t height , const size_t widht) :
Bird(" " , "White" , "WHITE",1,widht),
header_Tube(" ","White","WHITE",1,widht),
coin(" ","YELLOW","YELLOW",1,1),
Bird_ptr(0),
Player_ptr(0)
{
	this->SetFooteHeight(rand()%10+3);
}



void Tube ::  SetBGTube(Background color)
{
	this->SetBackground(color);
	this->header_Tube.SetBackground(color);
}


Tube :: Tube ():
Bird(" " , "White" , "LIGHTCYAN",1,3),
header_Tube(" ","White","LIGHTCYAN",1,3),
coin("*" , "Yellow" , "LIGHTCYAN",1,1),
Bird_ptr(0),
Player_ptr(0)
{
	this->coin=4;
	this->coin.SetBackground(LIGHTCYAN);
	this->coin.SetColor_sh(LightCyan);
	this->coin.SetColor(Yellow);
	this->SetFooteHeight(rand()%10+3);
	this->SetTubePosition(55,3);
}


void Tube :: SetFooteHeight(const size_t height)
{

	
	this->Heigth=height;
	this->Widht=3;
	

	char * tempStr = new char [height*this->Widht+1];						// array of char for Tube 
	char * tempShd = new char [height+1];							// array of char for shadow
	
	
	for (int i=0; i<height*this->Widht; i++)
		tempStr[i]=' ';
	tempStr[height*this->Widht]=0;

	for (int i=0; i<height; i++)
		tempShd[i]=' ';
	tempShd[height]=0;

	this->SetStr(tempStr);
	this->SetShadow(tempShd);

	delete [] tempStr;
	delete [] tempShd;




	// HEADER Tube
	//15
	tempStr = new char [(13-height)*this->Widht+1];						// array of char for Tube 
	tempShd = new char [13-height+1];							// array of char for shadow


	for (int i=0; i<(13-height)*this->Widht; i++)
		tempStr[i]=' ';
	tempStr[(13-height)*this->Widht]=0;

	for (int i=0; i<(13-height); i++)
		tempShd[i]=' ';
	tempShd[13-height]=0;


	this->header_Tube.SetStr(tempStr);
	this->header_Tube.SetShadow(tempShd);




	delete [] tempStr;
	delete [] tempShd;

	


	this->SetTubePosition(55,3);
}




Bird Tube :: GetBird()
{
	return this->header_Tube;

}

void Tube :: SetBirdPtr(Bird * ptr)
{
	this->Bird_ptr=ptr;
}

void Tube :: SetPlayerPtr(Player * ptr)
{
	this->Player_ptr=ptr;
}


void Tube :: SetTubePosition(const size_t x , const size_t y)
{
	this->header_Tube.SetMainPosition(x,y);
	this->header_Tube.SetPosToShadow(x+3,y);

	this->SetMainPosition(x,y+(17-this->Heigth));
	this->SetPosToShadow(x+3,y+(17-this->Heigth));
	
	this->coin.SetMainPosition(this->getX()+1,19-this->Heigth-1);
	this->coin.SetPosToShadow(this->getX()+2,19-this->Heigth-1);
}



void Tube :: PrintTube()
{

	this->PrintWithoutBorder();
	this->header_Tube.PrintWithoutBorder();
	this->PrintShd();
	this->header_Tube.PrintShd();

	this->coin.PrintWithoutBorder();
	this->coin.PrintShd();

}



size_t Tube :: GetWidth()
{
	return this->Widht;
}

bool Tube :: move (const char * direction, const size_t speed, const size_t delay)
{
	
	if (!strcmp(direction,"Up"))
		this->SetTubePosition(this->header_Tube.getX(),this->header_Tube.getY()-speed);


	else if (!strcmp(direction,"Left"))
		this->SetTubePosition(this->header_Tube.getX()-speed,this->header_Tube.getY());


	else if (!strcmp(direction,"Right"))
		this->SetTubePosition(this->header_Tube.getX()+speed,this->header_Tube.getY());

	else
		this->SetTubePosition(this->header_Tube.getX(),this->header_Tube.getY()+speed);

	Sleep(delay);
	this->PrintTube();
	if (this->getX()==5){

		
		this->SetBackground(LIGHTCYAN);
		this->header_Tube.SetBackground(LIGHTCYAN);

		this->coin.SetColor(LightCyan);
		this->PrintTube();
		this->SetFooteHeight(rand()%10+3);
		this->SetTubePosition(55,3);
		this->SetBGTube(GREEN);
		this->coin.SetColor(Yellow);
		
	}

	return this->Collision();
}



bool Tube :: Collision()
{
	size_t b_X = Bird_ptr->getX();
	size_t b_Y = Bird_ptr->getY();
	
	if 
		(((b_Y >= (this->coin.getY()-2)) && (b_Y <= (this->coin.getY()+1))) && 
		((b_X >= (this->coin.getX()-1)) && (b_X <= (this->coin.getX()+1))))
	{
		this->Player_ptr->SetScore(1);
		this->Player_ptr->PrintScore();
		this->coin.SetBackground(LIGHTCYAN);
		this->coin.SetColor(LightCyan);
	}

	if (b_Y<3 || b_Y>19)
		return false;

		if (b_X>=this->getX() &&
		b_X<=this->getX()+3 &&
		(b_Y<=19 && b_Y>=(20-this->Heigth)
		||
		b_Y>=5 && b_Y<=(18-3-this->Heigth)
		)
		)
		return false;

		return true;
}