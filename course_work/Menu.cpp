#include "Menu.h"




Menu::Menu() :
header("                        FLAPPY  Bird                        ", 6, 1, char(178), 20),
button_new_game("   New game   ", 9, 10, char(177), 14),
button_continue_game("Continue  game", 9, 15, char(177), 14),
button_exit("     Exit     ", 9, 20, char(177), 14),
mPlayer_ptr(0)
{
	this->header.SetColor(White);
	this->header.SetBackground(LIGHTBLUE);

	this->button_new_game.SetColor(Black);
	this->button_new_game.SetBackground(GREY);

	this->button_continue_game.SetColor(Black);
	this->button_continue_game.SetBackground(GREY);

	this->button_exit.SetColor(Black);
	this->button_exit.SetBackground(GREY);
}


void Menu::print_Menu()
{
	system("mode 36,26");
	this->header.print();
	this->button_new_game.SetColor(White);
	this->button_new_game.print();
	this->button_continue_game.SetColor(Black);
	this->button_continue_game.print();
	this->button_exit.print();
	int idx = 1;
	int i = 1;

	do
	{
		i = _getch();
		if (i == 72)
		{
			if (idx == 1){}
			else idx--;
		}
		if (i == 80)
		{
			if (idx == 3){}
			else idx++;
		}

		if (idx == 1)
		{
			this->button_new_game.SetColor(White);
			this->button_continue_game.SetColor(Black);
			this->button_new_game.print();
			this->button_continue_game.print();
		}
		else if (idx == 2)
		{
			this->button_continue_game.SetColor(White);
			this->button_new_game.SetColor(Black);
			this->button_exit.SetColor(Black);

			this->button_exit.print();
			this->button_continue_game.print();
			this->button_new_game.print();
		}
		else if (idx == 3)
		{
			this->button_exit.SetColor(White);
			this->button_continue_game.SetColor(Black);
			this->button_exit.print();
			this->button_continue_game.print();
		}

		if (i == 13 && idx == 3)
			exit(1);
		if (i == 13 && idx == 1)
			this->StartNewGame();
		if (i == 13 && idx == 2)
			this->ContinueAndHigthScores();


		i = 2;
	} while (i != 27);

}



void Menu::StartNewGame()
{
	system("cls");
	system("mode 80,25");
	if (!mPlayer_ptr){
		cout << "Enter Player name : ";
		char * plr_name = new char[25];
		gets(plr_name);
		cout << "Enter password: ";
		char * password = new char[25];
		gets(password);
		this->mPlayer_ptr = new Player(plr_name, 0, password);
	}

	system("cls");
	Display Display(21, 74, 17, 51, "Red", "CYAN", "LightGrey", "LIGHTCYAN");
	Display.SetDPlayerPtr(this->mPlayer_ptr);
	Display.print();
	system("cls");

	ofstream oFile;
	char * temp = new char[20];
	strcpy(temp, this->mPlayer_ptr->getName());
	strcat(temp, ".dat");
	oFile.open(temp, ios::binary);

	oFile << *(this->mPlayer_ptr);

	oFile.close();

	delete[]temp;
	this->mPlayer_ptr = 0;
	this->print_Menu();
}


Player *  Menu::GetPlayer()
{
	return this->mPlayer_ptr;
}

void Menu::ContinueAndHigthScores()
{
	system("cls");
	system("mode 36,40");

	Informer iChoise("                     Choise the Player:                     ", 6, 2, char(177), 20);
	iChoise.SetBackground(LIGHTBLUE);
	iChoise.print();

	vector<string> score_list;

	char * path = new char[20];
	path[0] = 0;
	strcpy(path, "*.dat");

	// ptr _finddata_t
	// new object _finddata_t
	_finddata_t *fileinfo = new _finddata_t;

	// start searching
	long done = _findfirst(path, fileinfo);
	int MayWeWork = done;

	//files quantity 
	int count = 0;
	while (MayWeWork != -1)
	{
		count++;
		// add filename to vector
		score_list.push_back(fileinfo->name);
		// search newt file
		MayWeWork = _findnext(done, fileinfo);
	}

	// clear memory
	_findclose(done);
	delete fileinfo;


	Player * Players_array = new Player[score_list.size()];
	int Y = 5;
	for (int i = 0; i < score_list.size(); i++)
	{
		XString file_name;
		file_name.SetStr(score_list[i]);
		ifstream iFile;
		iFile.open(file_name, ios::binary);
		iFile >> Players_array[i];
		iFile.close();

	}

	for (int i = 0; i < score_list.size(); i++)
		for (int j = 0; j < score_list.size(); j++)
		{
			if (Players_array[i].GetiScore() > Players_array[j].GetiScore())
			{
				Player temp = Players_array[i];
				Players_array[i] = Players_array[j];
				Players_array[j] = temp;
			}
		}
	for (int i = 0; i < score_list.size(); i++)
		Players_array[i].PrintPlayer(6, Y += 4);
	int i = 1;
	int idx = 0;
	int quantity = score_list.size();
	do
	{

		i = _getch();
		switch (i)
		{
		case 72:{
			if (idx)
			{
				Players_array[idx].SetColor(Black);
				Players_array[--idx].SetColor(White);
				Players_array[idx + 1].PrintPlayer(Players_array[idx + 1].getX(), Players_array[idx + 1].getY());
				Players_array[idx].PrintPlayer(Players_array[idx].getX(), Players_array[idx].getY());
			}
		} break;
		case 80:{
			if (idx < score_list.size() - 1)
			{
				Players_array[idx].SetColor(Black);
				Players_array[++idx].SetColor(White);
				Players_array[idx - 1].PrintPlayer(Players_array[idx - 1].getX(), Players_array[idx - 1].getY());
				Players_array[idx].PrintPlayer(Players_array[idx].getX(), Players_array[idx].getY());
			}
		} break;
		}
		if (i == 13)
		{
			if (Players_array[idx].GetContinue())
			{
				system("cls");
				cout << "Enter password: ";
				const char pSize = 20;
				char * tempPass = new char[pSize];
				gets(tempPass);
				if (!strcmp(tempPass, Players_array[idx].GetPassword()))
				{
					this->mPlayer_ptr = &Players_array[idx];
					system("cls");
					this->StartNewGame();

				}
				else
				{
					cout << "Wrong password!!!";
					Sleep(2000);
					print_Menu();
				}
			}

		}
	} while (i != 27);

	this->button_continue_game.SetColor(Black);
	system("cls");
	this->print_Menu();
}
