#pragma once 
#include "Bird.h"
#include "Player.h"


class Tube : public Bird
{
public:

	Tube ();																	//default constructor
	Tube (const char * newInf);													//constructor
	Tube (const char symbol , const size_t height , const size_t widht);		//constructor arguments ( symbol, Tube height, Tube wight)
	size_t GetWidth();															//get widht
	bool move (const char * direction, const size_t speed, const size_t delay);	// move arguments (direction(cosnt char *), speed, delay)
	void SetTubePosition(const size_t x , const size_t y);
	void PrintTube();
	void SetFooteHeight(const size_t height);
	Bird GetBird();
	void SetBGTube(Background color);
	bool Collision();
	void SetBirdPtr(Bird * ptr);
	void SetPlayerPtr(Player * ptr);
private:
	size_t	Widht;
	size_t Heigth;
	Bird coin;
	Bird header_Tube;
	Bird * Bird_ptr;
	Player * Player_ptr;
};