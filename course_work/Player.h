#pragma once 
#include "Bird.h"
#include <fstream>


class Player
{
public :
	Player();
	Player(const char * pl_name,const size_t scr, const char * plr_password);														//default constructor
	void SetScore(const size_t Score);													// set Player score
	void SetName(const char * name);
	void SetPassword(const char * password);
	void PrintName();
	void SetContinue(bool flag);
	bool GetContinue();
	void PrintScore();
	void PrintPlayer(size_t x,size_t y);
	void SetNamePos(size_t x, size_t y);
	void SetScorePos(size_t x, size_t y);
	size_t GetiScore();
	void FromIntToInf();
	char * getName();
	char * GetPassword();
	void SetColor(Color color);
	size_t getX();
	size_t getY();
	friend ofstream & operator << (ofstream & out, Player & obj );					//overloaded << 
	friend ifstream & operator >> (ifstream & in, Player & obj );						//overloaded >> 
	Player operator = (Player & arg);

private:
	Informer Player_name;
	Informer score;
	size_t i_score;
	XString password;
	bool CanContinue;
};