#pragma once
#include "Informer.h"
#include "Player.h"

class Display 
{
public:
	Display();					// default constructor
	Display(const size_t screen_height, 
		const size_t screen_width, 
		const size_t field_height,
		const size_t field_width,
		 char * Scolor,
		 char * SBGcolor,
		 char * Fcolor,
		 char * FBGcolor);					// constructor
	void print();
	void SetDPlayerPtr(Player * ptr);
private:
	int game_over;
	Informer screen;
	Informer field;
	Informer game_name;
	Player * dPlayer_ptr;
};