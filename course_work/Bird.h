#pragma once
#include "Informer.h"

class Bird : public Informer
{
public : 
	Bird(const char * value, char * shadowColor, char * shadowBackGround, const size_t shadow_widht, const size_t obj_widht);	// arguments (value str, shadow font color, shadow background, shadow widht, str widht)
	Bird operator = (const char * line);				// overloaded operator (const char * line)
	Bird operator = (char symbol);						// overloaded operator (char symbol)
	void SetShadow(const char * line);					// set shadow value
	void move (const char * direction, const size_t speed, const size_t delay);							//function to move the object in some direction (left,right,up,down). s
	void PrintShd();										//print shadow without borders
	void SetPosToShadow(const size_t x, const size_t y);	//set position to shadow
	void SetColor_sh(Color color);										//set color to shadow
	void SetBGColorShadow(Background BGcolor);										//set bkcolor to shadow
	void SetShadow();
private :
	Informer shadow;

};
