#include "Informer.h"


Informer :: Informer(const char * newInf){
	this->SetStr(newInf);
	this->SetMainPosition(0,0);
	this->symbol='+';
	this->hCon=GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo( this->hCon, &csbi );
	this->font_color=White;
	this->background=BLACK;

}

Informer :: Informer(const char * newInf,unsigned x,unsigned y,const char symbol,const unsigned Nmax_wight){
	this->SetStr(newInf);
	SetPosition(x,y);
	SetMainPosition(x,y);
	this->symbol=symbol;
	this->hCon=GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo( this->hCon, &csbi );
	this->font_color=White;
	this->background=BLACK;
	if(Nmax_wight>79) 
		this->max_wight=79;
	else 
		this->max_wight=Nmax_wight;
}

Informer :: Informer(const char * newInf,unsigned x,unsigned y,const char symbol,const unsigned Nmax_wight,Color c,Background b){
	this->SetStr(newInf);
	SetPosition(x,y);
	SetMainPosition(x,y);
	this->symbol=symbol;
	this->hCon=GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo( this->hCon, &csbi );
	this->font_color=c;
	this->background=b;
	if(Nmax_wight>79) 
		this->max_wight=79;
	else 
		this->max_wight=Nmax_wight;
}

void Informer :: SetPosition(unsigned x,unsigned y){
	Cpos.X=x;
	Cpos.Y=y;
	SetConsoleCursorPosition(this->hCon,Cpos);
}
void Informer :: SetMainPosition(unsigned x,unsigned y){
	MAIN_pos.X=x;
	MAIN_pos.Y=y;
	SetConsoleCursorPosition(this->hCon,Cpos);
}



void Informer :: print(){
	SetPosition(this->MAIN_pos.X,this->MAIN_pos.Y);
	
	GetConsoleScreenBufferInfo( this->hCon, &csbi );
	
	SetConsoleTextAttribute( this->hCon, this->font_color | this->background);
	
	unsigned length=strlen(this->getstr())+4;					//define the wight of the border
	if (length-4<=this->max_wight){
		for(int i=0;i<length;i++)									//print first bord
			cout<<this->symbol;

		SetPosition(this->Cpos.X,++this->Cpos.Y);							//set new position on new line

		cout<<this->symbol<<" "<<this->getstr()<<" "<<this->symbol;	//print text

		SetPosition(this->Cpos.X,++this->Cpos.Y);							// set new position

		for(int i=0;i<length;i++)									//second bord
			cout<<this->symbol;
		cout<<endl;
	}
	else {
		int line_amount=(length-4)/this->max_wight;					//define line amount
		if ((length-4)%max_wight) line_amount++;

		for(int i=0;i<this->max_wight+4;i++)					//print first bord
			cout<<this->symbol;
		size_t start_pos=Cpos.X;									//define start pos.X
		SetPosition(this->Cpos.X,++this->Cpos.Y);

		char * mtemp=0;											//main line copy
		mtemp=new char [strlen(this->getstr())+1];
		strcpy(mtemp,this->getstr());						

		for (int i=1;i<=line_amount;i++){				// main cycle
			cout<<this->symbol<<" ";
			char * temp=0;								//small line
			temp=new char [100];

			for(int i=0;i<this->max_wight;i++)			//formed small line
				temp[i]=mtemp[i];
			temp[this->max_wight]=0;

			cout<<temp<<" ";							//print small line
			
			unsigned l=strlen(temp);					//pos right board
			if (l<this->max_wight)
				for(int i=0;i<this->max_wight-l;i++)
					cout<<" ";
			
			cout<<this->symbol;
			SetPosition(start_pos,this->Cpos.Y+1);							//set new position on new line
			memmove(mtemp,mtemp+this->max_wight,strlen(mtemp+this->max_wight-1));

			delete []temp;
		}
		for(int i=0;i<this->max_wight+4;i++)						//print last bord
			cout<<this->symbol;
		cout<<endl;
		delete []mtemp;
	}
	
	
	
	
	SetConsoleTextAttribute(this->hCon,this->csbi.wAttributes);
}



void Informer :: PrintWithoutBorder(){
		SetPosition(this->MAIN_pos.X,this->MAIN_pos.Y);
	
	GetConsoleScreenBufferInfo( this->hCon, &csbi );
	
	SetConsoleTextAttribute( this->hCon, this->font_color | this->background);
	
	unsigned length=strlen(this->getstr())+2;					//define the wight of the border
	if (length-2<=this->max_wight){


		SetPosition(this->Cpos.X,++this->Cpos.Y);							//set new position on new line

		cout<<this->getstr();	//print text

		SetPosition(this->Cpos.X,++this->Cpos.Y);							// set new position

	
		cout<<endl;
	}
	else {
		int line_amount=(length-2)/this->max_wight;					//define line amount
		if ((length-2)%max_wight) line_amount++;

		
		size_t start_pos=Cpos.X;									//define start pos.X
		SetPosition(this->Cpos.X,++this->Cpos.Y);

		char * mtemp=0;											//main line copy
		mtemp=new char [strlen(this->getstr())+1];
		strcpy(mtemp,this->getstr());						

		for (int i=1;i<=line_amount;i++){				// main cycle
	
			char * temp=0;								//small line
			temp=new char [100];

			for(int i=0;i<this->max_wight;i++)			//formed small line
				temp[i]=mtemp[i];
			temp[this->max_wight]=0;

			cout<<temp;							//print small line
			
			unsigned l=strlen(temp);					//pos right board
			if (l<this->max_wight)
				for(int i=0;i<this->max_wight-l;i++)
					cout<<" ";
			
			
			SetPosition(start_pos,this->Cpos.Y+1);							//set new position on new line
			memmove(mtemp,mtemp+this->max_wight,strlen(mtemp+this->max_wight-1));

			delete []temp;
		}

		cout<<endl;
		delete []mtemp;
	}
	
	
	
	
	SetConsoleTextAttribute(this->hCon,this->csbi.wAttributes);

}



void Informer :: SetColor(Color Ncolor){
	this->font_color=Ncolor;
}

void Informer :: AddChar(char symbol)
{
	this->symbol=symbol;
}

void Informer :: SetBackground(Background Nbackground){
	this->background=Nbackground;
}


void Informer :: SetColor(char * str){
	this->SetColor(CheckColor(str));
}


void Informer :: SetBackground(char * str){
	this->SetBackground(CheckBackground(str));
}

Color Informer :: CheckColor (char * inString){
	if (!strcmp(inString,"Black")) return Black;
	if (!strcmp(inString,"Grey")) return Grey;
	if (!strcmp(inString,"LightGrey")) return LightGrey;
	if (!strcmp(inString,"White")) return White;
	if (!strcmp(inString,"Blue")) return Blue;
	if (!strcmp(inString,"Green")) return Green;
	if (!strcmp(inString,"Cyan")) return Cyan;
	if (!strcmp(inString,"Red")) return Red;
	if (!strcmp(inString,"Purple")) return Purple;
	if (!strcmp(inString,"LightBlue")) return LightBlue;
	if (!strcmp(inString,"LightGreen")) return LightGreen;
	if (!strcmp(inString,"LightCyan")) return LightCyan;
	if (!strcmp(inString,"Orange")) return Orange;
	if (!strcmp(inString,"LightPurple")) return LightPurple;
	if (!strcmp(inString,"Yellow")) return Yellow;
}

Background Informer :: CheckBackground(char * inString){
	if (!strcmp(inString,"BLACK")) return BLACK;
	if (!strcmp(inString,"GREY")) return GREY;
	if (!strcmp(inString,"LIGHTGREY")) return LIGHTGREY;
	if (!strcmp(inString,"WHITE")) return WHITE;
	if (!strcmp(inString,"BLUE")) return BLUE;
	if (!strcmp(inString,"GREEN")) return GREEN;
	if (!strcmp(inString,"CYAN")) return CYAN;
	if (!strcmp(inString,"RED")) return RED;
	if (!strcmp(inString,"PURPLE")) return PURPLE;
	if (!strcmp(inString,"LIGHTBLUE")) return LIGHTBLUE;
	if (!strcmp(inString,"LIGHTGREEN")) return LIGHTGREEN;
	if (!strcmp(inString,"LIGHTCYAN")) return LIGHTCYAN;
	if (!strcmp(inString,"ORANGE")) return ORANGE;
	if (!strcmp(inString,"LIGHTPURPLE")) return LIGHTPURPLE;
	if (!strcmp(inString,"YELLOW")) return YELLOW;
}


Informer Informer :: operator = (Informer & arg){
	this->SetStr(arg.getstr());
	return *this;
}
Informer Informer :: operator = (const char * line){
	this->SetStr(line);
	return *this;
}

Informer Informer :: operator += (Informer & arg){
	char * text=0;
	text=new char [strlen(this->getstr())+strlen(arg.getstr())+1];
	strcpy(text,this->getstr());
	strcat(text,arg.getstr());
	this->SetStr(text);
	delete []text;
	return *this;
}
Informer Informer :: operator = ( char symbol)
{
	char * text= new char [2];
	text[0]=symbol;
	text[1]=0;
	this->SetStr(text);
	delete []text;
	return *this;
}

Informer Informer :: operator += (const char * line){
	char * text=0;
	text=new char [strlen(this->getstr())+strlen(line)+1];
	strcpy(text,this->getstr());
	strcat(text,line);
	this->SetStr(text);
	delete []text;
	return *this;

}

Informer Informer :: operator += (const char symbol){
	unsigned length=strlen(this->getstr());
	char * temp;
	temp=new char[length+2];
	for(int i=0;i<length;i++)
		temp[i]=this->getstr()[i];
	temp[length]=symbol;
	temp[length+1]=0;
	this->SetStr(temp);
	delete []temp;
	return *this;
}


void Informer :: update(){
	unsigned choice=1;

	do{
		cout<<"1 - Edit all text\n"
			<<"2 - Add text\n"
			<<"3 - Add symbol\n"
			<<"4 - Edit font color\n"
			<<"5 - Edit background\n"
			<<"6 - Edit coordinates\n"
			<<"7 - Edit border\n"
			<<"8 - Exit\n";
		cin>>choice;
		switch(choice){
		case 1 : {									// new text
			cout<<"Enter new text: ";
			char * text=0;
			text=new char [256];
			cin.ignore(1);
			gets(text);
			*this=text;
			delete []text;
			system("cls");
			this->print();
				 }break;
		case 2 : {									//add text
			cout<<"Enter text: ";	
			char * text=0;
			text=new char [256];
			cin.ignore(1);
			gets(text);
			*this+=text;
			delete []text;
			system("cls");
			this->print();
				 }break;
		case 3 : {									//add symbol
			cout<<"Enter symbol: ";
			char symbol;
			cin>>symbol;
			*this+=symbol;
			system("cls");
			this->print();
				 }break;
		case 4 : {									// change font color
			char * color=0;
			color=new char [15];
			cout<<"Enter color for change font: ";
			cin.ignore(1);
			gets(color);
			this->SetColor(color);
			delete []color;
			system("cls");
			this->print();
				 }break;
		case 5 : {									// change background color
			char * color=0;
			color=new char [15];
			cout<<"Enter color for change background: ";
			cin.ignore(1);
			gets(color);
			this->SetBackground(color);
			delete []color;
			system("cls");
			this->print();
				 }break;
		case 6:{
			size_t x,y;
			cout<<"Enter x and y: ";
			cin>>x>>y;
			SetMainPosition(x,y);
			system("cls");
			this->print();
			   }break;
		case 7 :{
			char s;
			cout<<"Enter new board: ";
			cin>>s;
			this->symbol=s;
			system("cls");
			this->print();
				}break;
		default : system("cls");break;
		}
	}while(choice>0 && choice <8);
}


size_t Informer:: getX()
{
	return this->MAIN_pos.X;

}

size_t Informer:: getY()
{
	return this->MAIN_pos.Y;

}